export class Event {
    id: string;
    icon: string;
    title: string;
    place: Place;
    promotion: Promotion;
    description: string;
    category: string;
    detailLink: string;
    startTime: number;
    coverPhoto: string;
    stories: string[];
    hasPromo: boolean; //eventueel ternaire operator op null / empty, nog opzoeken
    hasPictures: boolean; //eventueel ternaire operator op null / empty, nog opzoeken
    date: Date;
}

export class Place {
    name: string;
    address: string;
}

export class Promotion {
    name: string;
    shortName: string;
}