import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent} from "./home/home.component";
import {AboutComponent} from "./about/about.component";
import {EventsComponent} from "./events/events.component";
import {CarouselComponent} from "./carousel/carousel.component";

const routes: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'events',
        component: EventsComponent
    },
    {
        path: 'about',
        component: AboutComponent
    },
    {
        path: 'events/:id',
        component: CarouselComponent,
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
