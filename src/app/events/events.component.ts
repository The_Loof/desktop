import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import{Event} from "../models/event.model";
declare let $ : any;
import {EventService} from "./event.service";
import { fadeInAnimation } from '../animations/fade-in.animation';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' }
})
export class EventsComponent implements OnInit {

   // events: Observable<Event[]>; | async pipe toevoegen via deze manier

   events: Event[];

  readonly ROOT_URL =  "https://on-the-moment-dev.herokuapp.com/external/events";

  constructor(private http: HttpClient, public eventService: EventService) {

  }

    ngOnInit() {
        this.events = Array<Event>();
        this.getEvents().subscribe(value =>{
            this.getEveryEventDetail(value)
        });
    }

  getEvents() { //returnen hier de observable ipv async te linken in de html omdat we nog de detaildata moeten inladen van de events voor we deze gaan linken
     return this.http.get<Event[]>(this.ROOT_URL + "/upcoming");
  }

  getEveryEventDetail(data: Event[]){
      data.forEach((element: Event) => {
          this.http.get(<string>(this.ROOT_URL + "/" + element.id)).subscribe(value => {
              element.coverPhoto = value["coverPhoto"];
              element.stories = value["stories"];
              element.description = value["description"];
              element.detailLink = value["detailLink"];

              if (element.stories != null && element.stories.length > 0){
                  element.hasPictures = true;
              }

              if (element.promotion.name != null && element.promotion.name != ""){
                  element.hasPromo = true;
              }

              console.log(element.startTime);
              element.date = this.timeConverter(element.startTime);
              console.log(element.date);

              this.events.push(element);

              this.bindstuff('#accordion' + element.id, '#' + element.id, '#ph' + element.id);
          })

      });
  }

  saveStoriesService(stories: String[]): void{
      this.eventService.serviceData = stories;
  }

    timeConverter(UNIX_timestamp) : Date {
            let str = UNIX_timestamp.toString();

            str = str.slice(0, -3);
            str = parseInt(str);

            let date = new Date(str * 1000);

            return date;
    }

    getMonthAbr(m : number) : string {
        let month = ["Jan","Feb","Mar","Apr","May","June","July","Aug","Sept","Oct","Nov","Dec"];
        return month[m];
    }

    bindstuff(accordionId : string, expandId : string, coverId : string) {
        $(document).ready(function() {
            let button1 = $(accordionId);
            let content1 = $(expandId);

            button1.mouseenter(function() {
                $(coverId).fadeTo(500, 0.3);
                content1.queue('collapsequeue',function(){
                    content1.collapse('show');
                });
                if (!content1.hasClass("collapsing")) {
                    content1.dequeue("collapsequeue");
                }
            });


            button1.mouseleave(function() {
                $(coverId).fadeTo(500, 1);
                content1.queue('collapsequeue',function(){
                    content1.collapse('hide');
                });
                if (!content1.hasClass("collapsing")) {
                    content1.dequeue("collapsequeue");
                }
            });
            content1.on("shown.bs.collapse hidden.bs.collapse", function(){
                content1.dequeue("collapsequeue");
            });

        });
    }

    }