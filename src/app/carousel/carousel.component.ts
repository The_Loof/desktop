import { Component, OnInit } from '@angular/core';
import {EventService} from "../events/event.service";
import {ActivatedRoute, Router} from "@angular/router";
declare let $ : any;

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  stories: String[];

  constructor(eventService: EventService, private router: Router, private route: ActivatedRoute) {
      this.stories = eventService.serviceData;
      if (this.stories == null || this.stories.length<=0 ){
          this.router.navigate(['/events']);
      }
  }

  ngOnInit() {
      this.checkItem();
      //weet dat het dupe code is maar voor een of andere reden wou het anders nie werekn
      $('#stories-carousel').on('slid.bs.carousel',function () {
          console.log("inline");
          let $this = $('#stories-carousel');
          if ($('.carousel-item:first').hasClass('active')) {
              $this.children('.carousel-control-prev').hide();
              $this.children('.carousel-control-next').show();
          } else if ($('.carousel-item:last').hasClass('active')) {
              $this.children('.carousel-control-next').hide();
              $this.children('.carousel-control-prev').show();
          } else {
              $this.children('.carousel-control-prev').show();
              $this.children('.carousel-control-next').show();
          }
      });

  }

    checkItem()
    {
        console.log("func");
        let $this = $('#stories-carousel');
        if ($('.carousel-item:first').hasClass('active')) {
            $this.children('.carousel-control-prev').hide();
            $this.children('.carousel-control-next').show();
        } else if ($('.carousel-item:last').hasClass('active')) {
            $this.children('.carousel-control-next').hide();
            $this.children('.carousel-control-prev').show();
        } else {
            $this.children('.carousel-control-prev').show();
            $this.children('.carousel-control-next').show();
        }

        if (this.stories.length == 1) {
            $this.children('.carousel-control-prev').hide();
            $this.children('.carousel-control-next').hide();
        }
    }

  closeStories() {
      this.router.navigate(['/events']);
  }

}
