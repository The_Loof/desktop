import { Component, OnInit } from '@angular/core';

// import fade in animation
import { fadeInAnimation } from '../animations/fade-in.animation';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class AboutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  masonry(grid, gridCell, gridGutter, dGridCol, tGridCol, mGridCol) {
      var g = document.querySelector(grid),
          gc = document.querySelectorAll(gridCell),
          gcLength = gc.length,
          gHeight = 0,
          i;

      for(i=0; i<gcLength; ++i) {
          gHeight+=gc[i].offsetHeight+parseInt(gridGutter);
      }

      if(window.screen.width >= 1024)
          g.style.height = gHeight/dGridCol + gHeight/(gcLength+1) + "px";
      else if(window.screen.width < 1024 && window.screen.width >= 768)
          g.style.height = gHeight/tGridCol + gHeight/(gcLength+1) + "px";
      else
          g.style.height = gHeight/mGridCol + gHeight/(gcLength+1) + "px";
  }

}
